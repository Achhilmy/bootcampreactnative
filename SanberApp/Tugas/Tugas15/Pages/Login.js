import React from 'react'
import { useEffect } from 'react'
import { StyleSheet, Text, View, Button } from 'react-native'
import { useSelector } from 'react-redux'

export default function Login({navigation}) {
    //const glovalState= useSelector(state=>state)
    const LoginReducer = useSelector(state=> state.LoginReducer)
    useEffect(()=>{
        console.log(LoginReducer)
    },[LoginReducer])
    return (
        <View style={styles.container}>
            {/* <Text>Hello, {glovalState.name}</Text> */}
            <Text>Hello, {LoginReducer.info}</Text>
            <Button onPress={()=>navigation.navigate("MyDrawwer",{
                screen : 'App', params:{
                    screen:'Aboutscreen'
                }
            })} 
            title="Menuju Halaman HomeScreen" />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems:'center'
    }
})
 