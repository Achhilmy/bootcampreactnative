import { DrawerItem } from '@react-navigation/drawer'
import React, { useEffect } from 'react'
import { useState } from 'react'
import { StyleSheet, Text, View, Image, Button } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { interpolate } from 'react-native-reanimated'
import { Data }from './data'
export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat=(num)=> {
        return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      };

    const updateHarga =(price)=>{
        console.log("UpdatPrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
      
    }
    // useEffect(() => {
    //     console.log(username)
    // }, [])

    
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
           <FlatList 
                data={Data}
                numColumns={2}
                keyExtractor={item=> item.id }
                showsVerticalScrollIndicator={false}
                renderItem={({item})=>{
                    return(
                        <View style={styles.content}>
                             <Text>{item.title}</Text> 
                             <Image style={{width: 120, height: 100, alignSelf: 'center'}} source={item.image}/>
                             <Text>{item.harga}</Text>
                             <Text>{item.type} </Text>
                             <Button onPress={()=>updateHarga(item.harga)} title="Beli"/>
                        </View>
                    )
                }}
            />
              </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'white', 
    },  
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',
    
    },
        
})
